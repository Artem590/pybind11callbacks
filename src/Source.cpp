#define _CRT_SECURE_NO_WARNINGS
#define NOMINMAX

#include <functional>
#include <string>
#include <thread>
#include <windows.h>
#include <mutex>

#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

using namespace std;

#define INTERRUPTION_POINT(flag) {mutex m; m.lock(); if (*flag) return 0; m.unlock();}

struct interruptible_thread {
	shared_ptr<thread> thread;
	shared_ptr<bool> interruptFlag;

	interruptible_thread(const shared_ptr<std::thread> & thr,
							const shared_ptr<bool> & flag) {
		this->thread = thr;
		this->interruptFlag = flag;
	}
};

void interruptThread(interruptible_thread thread) {
	mutex m; 
	m.lock();
	*thread.interruptFlag = true;
	m.unlock();
}

interruptible_thread getSeriesSum(int64_t n, function<void(double sum)> callback) {
	
	shared_ptr<bool> interruptionFlag(new bool);
	*interruptionFlag = false;
	 
	thread * thr = new thread([n, callback, interruptionFlag]() {
		double sum = 0;
		
		INTERRUPTION_POINT(interruptionFlag)

		for (int64_t i = 0; i < n; ++i)
			sum += cos(exp(-i)) * sin(i);

		INTERRUPTION_POINT(interruptionFlag)

		callback(sum);
	});
	thr->detach();

	return interruptible_thread(shared_ptr<thread>(thr), interruptionFlag);
}

PYBIND11_MODULE(pybind11Test, m) {

	py::class_<interruptible_thread>(m, "interruptible_thread");

	m.def("interruptThread", &interruptThread);
	m.def("getSeriesSum", &getSeriesSum);
}